$(function(){
    $('.tabs_menu a').click(function(e) {
        e.preventDefault();
        $('.tabs_menu .active').removeClass('active');
        $(this).addClass('active');
        var tab = $(this).attr('href');
        $('.tab').not(tab).css({'display':'none'});
        $(tab).fadeIn(600);
    });
});

$(function(){
	$(".filter a").on("click touchstart", function(e){
		$(this).toggleClass("active");
		e.preventDefault();
	});
});

$(function() {
	$(".small-item-image").hover(function(){
		$(this).children(".small-item-hover").show("slide", {"direction":"down"}, 300);
	}, function(){
		$(this).children(".small-item-hover").hide("slide", {"direction":"down"}, 200);	
	});
});