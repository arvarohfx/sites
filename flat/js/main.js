//Функция перемотки вверх страницы
$(function() {
	$(".scroll-up").click(function(e) {
        if( !$("body,html").is(':animated') ) {     //Если прокрутка не осуществляется => прокрутить
            var curPos = $(document).scrollTop();
            var scrollTime=curPos/1.73;
            $("body,html").animate({"scrollTop":0}, scrollTime);
            e.preventDefault();
        }	
    });
});

//Перемотка по разделам
$(function(){
    $('.navigation a').click( function(){ 
	var scroll_el = $(this).attr('href'); 
        if ($(scroll_el).length != 0) { 
	    $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500); 
        }
	    return false;
    });
});

//Social dropdown
$(function() {
	
	$(".avatar").hover(function(){
		var target = $(this).children(".social");
		
		target.stop(1, 1).show("drop", {"direction":"up"}, 300);
	},
	function () {
		var target = $(this).children(".social");
		
		target.stop(1, 1).hide("drop", {"direction":"up"}, 300);
	});
});

// РАБОТА МОБИЛЬНОГО САЙДБАРА
var sidebarMenu = function() { //главная функция 
    $('.icon-menu').click(function() {  
        $('.menu').animate({right: '0px'}, 400, "easeOutBounce"); 
        $("body").css("position", "absolute");
        $('body').animate({right: '285px'}, 400, "easeOutBounce");
    });
 
 
/* Закрытие меню */
 
    $('.icon-close').click(function() {  
        $('.menu').animate({right: '-285px'}, 400, "easeOutBounce"); 
    	$('body').animate({right: '0px'}, 400, "easeOutBounce");
    	

    	setTimeout(function(){$("body").css("position", "static");},400);
    	  
    });
};
 
$(document).ready(sidebarMenu); /* как только страница полностью загрузится, будет
               вызвана функция main, отвечающая за работу меню */