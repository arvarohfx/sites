$(function(){
    $(".modal").modal('show');
});

//Если скрипт на клиентской стороне не работает, то поиск будет виден, иначе скрыть всё
$(function() {
	$(".search-block").css("display", "none");		//скрыть блок поиска
	$(".search-block").css("opacity", "0"); 		//уменьшить прозрачность до нуля блока поиска
	$(".search-block").css("width", "0"); 			//уменьшить до нуля ширину блока поиска	

	$(".scroll-top").fadeOut(0); 					//скрыть блок скролла
    
});


//===================================================================================
//Показать/скрыть элемент скролла при прокрутке
$(function() {
    $(window).scroll(function(){
        if (window.innerWidth > 1500) {            //Если ширина окна больше 1500 => Показать блок
            if ( window.pageYOffset < 500) {       //Если высота меньше 500, скрыть
                $(".scroll-top").fadeOut(600);    		
            } else {                               //Иначе => Показать
                $(".scroll-top").fadeIn(600);
            };  
        }  
    });

    $(window).resize(function() {                   //Скрыть блок при ресайзе страницы
        if (window.innerWidth > 1500) { 
            $(".scroll-top").fadeOut(200);
        }
    })

    //Прокрутка вверх по клику
    $(".scroll-top").click(function() {
        if( !$("body,html").is(':animated') ) {     //Если прокрутка не осуществляется => прокрутить
            var curPos = $(document).scrollTop();
            var scrollTime=curPos/1.73;
            $("body,html").animate({"scrollTop":0}, scrollTime);
        }	
    });
});
//===================================================================================


//===================================================================================
//Показать/скрыть поиск
$(function() {
    $(".search-btn").on("click", function(e) {	
        if ( !$(".search-block").is(':animated') ) {
            $(".search-block").css("display", "table");
            $(".search-block").animate({opacity: ".9", width: "250"}, 500);
            e.stopPropagation();
            e.preventDefault(); 
        }

    });

    $(document).on('click touchstart', function(e) {
        if (!$(e.target).closest(".search").length) {
            if ( !$(".search-block").is(':animated') ) {
                $('.search-block').animate({opacity: "0", width: "0"}, 400);

                setTimeout(function() {
                    $('.search-block').css("display", "none");
                    $(".search-block .form-control").val('');				//Очистить инпут поиска
                }, 400);
            }
        }
        e.stopPropagation();
    });
});
//===================================================================================


//===================================================================================
//При прокрутке вниз сделать шапку полупрозрачной
$(function() {
    $(window).scroll(function(){
        if ( window.pageYOffset < 500 ) {
            $("header").css("opacity", "1"); 
        } else { 
            $("header").css("opacity", ".93"); 
        };
    });
});   
//===================================================================================


//===================================================================================
//{АДАПТИВНОЕ МЕНЮ}
$(function() {
	$(".menu-trigger").on("click", function(){
		if (!$(".navigation ul").is(':animated')){
			$(".navigation ul").slideToggle(500);					
		}
		return false;
	});

	$(window).resize(function(){
		if ($(window).width() > 500) {
			$('.navigation ul').removeAttr('style');
		}
	});

	$( document ).on('click touchstart', function(e) {
  		if ( !$(e.target).closest(".navigation ul").length && $(window).width() < 991)  {
    			$(".navigation ul").slideUp(500);
  			}
  		e.stopPropagation();
  		
	});
});
//КОНЕЦ {АДАПТИВНОЕ МЕНЮ}
//===================================================================================


//===================================================================================
//Показать/скрыть Dropdown меню 
$(function() {
    $(".dropdown-target").on("mouseenter touchstart", function(e) {
        var target=e.target;


        if( !$(".dropdown").is(':animated') && window.innerWidth > 991 ) {
            $(".dropdown").slideDown(200);
            $("header").css("opacity", "1");
        }
        e.stopPropagation();
    });

    $( document ).on('click touchstart', function(e) {
        if ( !$(e.target).closest(".navigation").length )  {
                $(".dropdown").slideUp(200);
            }
        e.stopPropagation();
    });

    $(window).resize(function() {
        $(".dropdown").slideUp(200);
    })

    $(".navigation").on("mouseleave", function(e) {
            $(".dropdown").slideUp(200);
            e.stopPropagation();
    });
});
//====================================================================================

//====================================================================================
//РАБОТА СЛАЙДЕРА
$(function() {
	var imgs = $("#slider img"),				//Все изображения слайдера
		//btns = $(".slider-round-btns button")	//Все круглые кнопки
		indexImg = 1,							//Индекс изображения (1 - первое)
		indexMax = 3,                 //Максимальный индекс изображений (кол-во)
        sliderSpeed = 6000,                     //Скорость слайдера в ms
        fadeAnimationSpeed = 600,                   //Скорость анимации исчезнования
        sliderThumbnails = $(".slider-thumbnail");
    
        sliderThumbnails.eq(indexImg-1).toggle("slide", {"direction":"left","distance":"0"}, fadeAnimationSpeed);        
                    //При загрузке показать с анимацией
    
    
    $('.slider-progress').stop();
    $('.slider-progress').css('width', '0%');
    $('.slider-progress').animate({width: "100%"}, 6000, 'linear');
                    //При загрузке анимировать прогресс-бар
    
//Функция автоматической смены изображений
		function autoChange() {
			indexImg++;
			if (indexImg > indexMax) {			//Если индекс больше количества изображений -> переходим к первому
				indexImg = 1;
			}           
            
			sliderSpin(indexImg);				//Функция анимации и смены изображений
            
		}

//Функция смены изображения при нажатии на кнопки
		function imgChange(direction) {
			if (direction == "next") {
				(indexImg >= indexMax) ? indexImg = 1 : indexImg++;
				sliderSpin(indexImg);
			} else if (direction == "prev") {
				(indexImg <= 1) ? indexImg = indexMax : indexImg--;
				sliderSpin(indexImg);
			}
		}

//Функция анимации и смены изображений
		function sliderSpin(indexImg) {
			imgs.fadeOut(600);
			imgs.filter(':nth-child('+indexImg+')').fadeIn(fadeAnimationSpeed);
			//btns.removeClass("active");
			//btns.filter(':nth-child('+indexImg+')').toggleClass("active");
                    
            
            
            $('.slider-progress').stop();
            $('.slider-progress').css('width', '0%');
            $('.slider-progress').animate({width: "100%"}, 6000, 'linear');
            if ($(window).width() >= 768) {
                sliderThumbnails.fadeOut(300);
                sliderThumbnails.eq(indexImg-1).toggle("slide", {"direction":"left","distance":"0"}, fadeAnimationSpeed);
            }
		}

	var timerID = setInterval(autoChange, sliderSpeed);


//Обработка кнопок NEXT PREV
	$("div#show")
		.show()
		.find("button")
		.on("click", function() {
			if( !imgs.is(':animated') ) {
				var direction = $(this).attr("id");
				imgChange(direction);
				clearInterval(timerID);
				timerID = setInterval(autoChange, sliderSpeed);
			}
	});

//Обработка круглых кнопок-переключателей
	$(".slider-round-btns button")
		.on("click", function(){
			if ( !$(this).hasClass("active") && !imgs.is(':animated') ) {
				indexImg = $(this).index()+1;
				sliderSpin(indexImg);
				clearInterval(timerID);
				timerID = setInterval(autoChange, sliderSpeed);
			}			

    });
    

    
});

$(function(){
	if ($(window).width() < 768) {
		$('#slider').height(function(){
			return $(this).children("img").height();
		});
		$(".slider-thumbnail").fadeOut(0);
	}

	$(window).resize(function(){
		if ($(window).width() < 768) {
			$('#slider').height(function(){
				return $(this).children("img").height();
			});
            $(".slider-thumbnail").fadeOut(0);
		} else {
			$("#slider").css('height', '');  //Вернуть значение по умолчанию
		}

	});
});
//END OF SLIDER
//===================================================================================
