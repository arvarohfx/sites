$(function(){

//Анимация ссылок
	$('h4>a, .events-title a').hover(function(){
		$(this).stop().animate({opacity: ".6"}, 400);
	},
	function(){
		$(this).stop().animate({opacity: "1"}, 400);
	});

//Анимация изменения цвета кнопок слайдера
	$('.controls a').hover(function(){
		$(this).stop().animate({color: "#00c0ff",
								borderColor: "#00c0ff"}, 400);
	},
	function(){
		$(this).stop().animate({color: "",
								borderColor: ""}, 400);
	});


//Анимация изменения цвета кнопки галлереи
	$(".gallery a").hover(function(){
		$(this).stop().animate({borderColor: '#272727', color: '#272727'}, 400);
	},
	function(){
		$(this).stop().animate({borderColor: '', color: ''}, 400);
	});

//Отмена действия по-умолчанию для кнопок календяра и событий
	$('.events-switch a, .calendar-next, .calendar-prev').click(function(e){
		e.preventDefault();
	});

//Выбор даты в календаре и анимация выбора
	// $("tbody tr td").find("span.active").css({
	// 	backgroundColor: "#00c0ff",
	// 	color: "#fff"
	// });
	// $("tbody tr td span").on("click", function() {
	// 	if ( !$(this).hasClass("disabled") && !$(this).hasClass("active") ) {
	// 		$("tbody tr td").find("span.active").animate({
	// 			backgroundColor: "transparent",
	// 			color: "#22262e"}, 0);
	// 		$("tbody tr td span").removeClass("active");
	// 		$(this).addClass("active").animate({
	// 			backgroundColor: "#00c0ff",
	// 			color: "#fff"}, 300);
	// 		}
	// });

});


// $(function(){

// 	var origOffsetY = $(".navigation").offset().top;

// 	function scroll() {
//         if ($(window).scrollTop() <= origOffsetY && $(window).width() < 768) {
//             $('.navigation').addClass('sticky').slideDown(200);
//             $('.content').addClass('menu-padding');
//         } else {
//             $('.navigation').removeClass('sticky');
//             $('.content').removeClass('menu-padding');
//         }
//     }

//     	document.onscroll = scroll;


// });



//====================================================================================
//РАБОТА СЛАЙДЕРА
$(function() {
	var imgs = $("#slider img"),				//Все изображения слайдера
		indexImg = 1,							//Индекс изображения (1 - первое)
		indexMax = 3,                 //Максимальный индекс изображений (кол-во)
        sliderSpeed = 6000,                     //Скорость слайдера в ms
        fadeAnimationSpeed = 600;                   //Скорость анимации исчезнования

        $('.slider-progress').stop();
    	$('.slider-progress').css('width', '0%');
    	$('.slider-progress').animate({width: "100%"}, 6000, 'linear');
                    //При загрузке анимировать прогресс-бар

        var bgCol = [
        	"#00d5c6",
        	"#01c0dd",
        	"#027ad9"
        ];
    
    
//Функция автоматической смены изображений
		function autoChange() {
			indexImg++;
			if (indexImg > indexMax) {			//Если индекс больше количества изображений -> переходим к первому
				indexImg = 1;
			}            
			sliderSpin(indexImg);				//Функция анимации и смены изображений            
		}

//Функция смены изображения при нажатии на кнопки
		function imgChange(direction) {
			if (direction == "next") {
				(indexImg >= indexMax) ? indexImg = 1 : indexImg++;
				sliderSpin(indexImg);
			} else if (direction == "prev") {
				(indexImg <= 1) ? indexImg = indexMax : indexImg--;
				sliderSpin(indexImg);
			}
		}

//Функция анимации и смены изображений
		function sliderSpin(indexImg) {
			imgs.fadeOut(fadeAnimationSpeed);
			imgs.filter(':nth-child('+indexImg+')').fadeIn(fadeAnimationSpeed);

			//$(".slider-wrap").css({"background-color": bgCol[indexImg-1]});

			$('.slider-progress').stop();
            $('.slider-progress').css('width', '0%');
            $('.slider-progress').animate({width: "100%"}, 6000, 'linear');
		}

	var timerID = setInterval(autoChange, sliderSpeed);


//Обработка кнопок NEXT PREV
	$("div#show")
		.show()
		.find("a")
		.on("click", function(e) {
			if( !imgs.is(':animated') ) {
				var direction = $(this).attr("id");
				imgChange(direction);
				clearInterval(timerID);
				timerID = setInterval(autoChange, sliderSpeed);
			}
			e.preventDefault();
	});    
});

// $(function(){
// 	$('.slider-wrap').height(function(){
// 			return $(this).children("img").height();
// 		});
// });
//END OF SLIDER
//==============================SIDE BAR MOBILE MENU=======================================
var sidebarMenu = function() { //главная функция 
    $('.icon-menu').click(function() {  
        $('.menu').animate({right: '0px'}, 400, "easeOutBounce"); 
        $("body").css("position", "absolute");
        $('body').animate({right: '285px'}, 400, "easeOutBounce");
    });
 
 
/* Закрытие меню */
 
    $('.icon-close').click(function() {  
        $('.menu').animate({right: '-285px'}, 100, "linear"); 
    	$('body').animate({right: '0px'}, 100, "linear");
    	

    	setTimeout(function(){$("body").css("position", "static");},400);
    	  
    });
};
 
$(document).ready(sidebarMenu); /* как только страница полностью загрузится, будет
               вызвана функция main, отвечающая за работу меню */




//==========КАЛЕНДАРЬ==========================
calendar = {};
 
// Названия месяцев
calendar.monthName=[
  	'January', 'February', 'March', 'April',
  	'May', 'June', 'July', 'August',
  	'September', 'October', 'November', 'December'
];
 
// Названия дней недели
calendar.dayName=[
  	'MON', 'TUE', 'WED', 'THU', 'FRI', 'SUN', 'SAT'
];

// Выбранная дата
calendar.selectedDate = {
  	'Day' : new Date().getDate(),
  	'Month' : parseInt(new Date().getMonth())+1,
  	'Year' : new Date().getFullYear()
};
 
// ID элемента для размещения календарика
calendar.element_id='calendar';
 
// Выбор даты
calendar.selectDate = function(day,month,year) {
  	calendar.selectedDate={
    	'Day' : day,
    	'Month' : month,
    	'Year' : year
  	};

  	calendar.drawCalendar(month,year);
}

// Отрисовка календарика на выбранный месяц и год 
calendar.drawCalendar = function(month,year) { 
	var tmp = '';
	var tmp2= '';
	tmp += '<table>'; 

	//Месяц и навигация 
	tmp2 += '<p class="calendar-month">'+ calendar.monthName[(month-1)] +'</p>'
	tmp2 += '<p class="calendar-year">'+ year +'</p>'
	tmp2 += '<p class="calendar-today">Today is <span>'+ new Date().getDate() +' '
				+calendar.monthName[parseInt(new Date().getMonth())].substr(0,3)+' '
				+new Date().getFullYear() +'</span></p>'

	tmp2 += '<a class="calendar-prev" onclick="calendar.drawCalendar('+(month>1?(month-1):12)+ 
	   ','+(month>1?year:(year-1))+');"><i class="fa fa-caret-left"></i><\/a>'; 

	tmp2 += '<a class="calendar-next" onclick="calendar.drawCalendar('+(month<12?(month+1):1)+ 
	   ','+(month<12?year:(year+1))+');"><i class="fa fa-caret-right"></i><\/a>'; 



	// Шапка таблицы с днями недели 
	tmp += '<thead>'; 
		tmp += '<tr>'
			tmp += '<td>'+calendar.dayName[0]+'<\/td>'; 
			tmp += '<td>'+calendar.dayName[1]+'<\/td>'; 
			tmp += '<td>'+calendar.dayName[2]+'<\/td>'; 
			tmp += '<td>'+calendar.dayName[3]+'<\/td>'; 
			tmp += '<td>'+calendar.dayName[4]+'<\/td>'; 
			tmp += '<td>'+calendar.dayName[5]+'<\/td>'; 
			tmp += '<td>'+calendar.dayName[6]+'<\/td>'; 
		tmp += "<\/tr>"
	tmp += '<\/thead>'; 

	// Количество дней в месяце 
	var total_days = 32 - new Date(year, (month-1), 32).getDate(); 
	// Начальный день месяца 
	var start_day = new Date(year, (month-1), 1).getDay(); 
	if (start_day == 0) { start_day = 7; } 
	start_day--; 
	// Количество ячеек в таблице 
	var final_index = Math.ceil((total_days+start_day)/7)*7;

	var day=1; 
	var index=0; 
	tmp += '<tbody>';
	  	do { 
		    // Начало строки таблицы 
		    if (index%7 == 0) { 
		      	tmp += '<tr>'; 
		    } 
		    // Пустые ячейки до начала месяца или после окончания 
		    if ((index < start_day) || (index >= (total_days + start_day))) { 
		      	tmp += '<td><span class="disabled"><\/span><\/td>'; 
		    } 
		    else { 
		      	var class_name = ''; 
		      	// Выбранный день 
		      	if (calendar.selectedDate.Day==day && 
		          	calendar.selectedDate.Month==month && 
		          	calendar.selectedDate.Year==year) { 
		        class_name = 'active'; 
		    } 
		    // Ячейка с датой 
		    tmp += '<td><span class="'+class_name+'" '+ 
		    	'onclick="calendar.selectDate('+ 
		    	day+','+month+','+year+');">'+day+'<\/span><\/td>'; 
		   		day++; 
			} 
		    // Конец строки таблицы 
		    if (index%7 == 6) { 
			    tmp+='<\/tr>'; 
			} 
				index++; 
		} 
	  	while (index < final_index); 
	  	tmp += '<\/tbody>';
	  	tmp += '<\/table>'; 
	  
	  // Вставить таблицу календарика на страницу 
	var el = document.getElementById(calendar.element_id); 
	  	if (el) { 
	    	el.innerHTML = tmp; 
	  	} 

	var el2 = document.getElementById(calendartop);
		if (calendartop) {
			calendartop.innerHTML = tmp2;
			console.log(calendartop);
		}

} 

calendar.drawCalendar(
	calendar.selectedDate.Month,
	calendar.selectedDate.Year
);


