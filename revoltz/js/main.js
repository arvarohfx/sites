$(function(){
	if ($(window).width() < 768) {
		$('.slider-wrap').height(function(){
			return $(this).children("img").height();
		});
	}

	$(window).resize(function(){
		if ($(window).width() < 768) {
			$('.slider-wrap').height(function(){
				return $(this).children("img").height();
			});
		} else {
			$(".slider-wrap").css('height', '');  //Вернуть значение по умолчанию
		}

	});
});


$(function() {
	$("ul.dd").css({display: "none"}); // Opera Fix
	if ($(window).width() >= 768) {
		$("li.dropdown").hover(function(){			
				$(this).find('ul.dd:first').css({
					visibility: "visible",
					display: "none"
					}).stop().fadeIn(200);
				
			},function(){
				$(this).find('ul.dd:first').css({
					visibility: "hidden"}).stop().fadeOut(200);
		});
	}
});

$(window).on('resize', function() {
	$("ul.dd").css({display: "none"}); // Opera Fix
	if ($(window).width() >= 768) {
		$("li.dropdown").hover(function(){
			$(this).find('ul.dd:first').css({
				visibility: "visible",
				display: "none"
				}).stop().fadeIn(200);
			},function(){
				$(this).find('ul.dd:first').css({
					visibility: "hidden"}).stop().fadeOut(200);
		});
	} else {
		$("li.dropdown").unbind();
	}

});


//РАБОТА СЛАЙДЕРА
$(function() {
	var imgs = $("#slider img"),				//Все изображения слайдера
		btns = $(".slider-round-btns a"),	//Все круглые кнопки
		indexImg = 1,							//Индекс изображения (1 - первое)
		indexMax = 4,                 //Максимальный индекс изображений (кол-во)
        sliderSpeed = 6000,                     //Скорость слайдера в ms
        fadeAnimationSpeed = 600,                   //Скорость анимации исчезнования
        sliderThumbnails = $(".slider-thumbnail");
    
        sliderThumbnails.eq(indexImg-1).toggle("slide", {"direction":"right","distance":"0"}, fadeAnimationSpeed);        
                    //При загрузке показать с анимацией

    
//Функция автоматической смены изображений
		function autoChange() {
			indexImg++;
			if (indexImg > indexMax) {			//Если индекс больше количества изображений -> переходим к первому
				indexImg = 1;
			}           
            
			sliderSpin(indexImg);				//Функция анимации и смены изображений
            
		}

//Функция смены изображения при нажатии на кнопки
		function imgChange(direction) {
			if (direction == "next") {
				(indexImg >= indexMax) ? indexImg = 1 : indexImg++;
				sliderSpin(indexImg);
			} else if (direction == "prev") {
				(indexImg <= 1) ? indexImg = indexMax : indexImg--;
				sliderSpin(indexImg);
			}
		}

//Функция анимации и смены изображений
		function sliderSpin(indexImg) {
			imgs.fadeOut(fadeAnimationSpeed);
			imgs.filter(':nth-child('+indexImg+')').fadeIn(fadeAnimationSpeed);

			btns.removeClass("active");
			btns.filter(':nth-child('+indexImg+')').toggleClass("active");

            sliderThumbnails.fadeOut(0);
            sliderThumbnails.eq(indexImg-1).toggle("slide", {"direction":"right","distance":"0"}, fadeAnimationSpeed);
            
		}

	var timerID = setInterval(autoChange, sliderSpeed);

//Обработка круглых кнопок-переключателей
	$(".slider-round-btns a")
		.on("click", function(){
			if ( !$(this).hasClass("active") && !imgs.is(':animated') ) {
				indexImg = $(this).index()+1;
				sliderSpin(indexImg);
				clearInterval(timerID);
				timerID = setInterval(autoChange, sliderSpeed);
			}	
    });
    
});

// КОНЕЦ СЛАЙДЕРА

//===================Боковое меню===========================================
var sidebarMenu = function() { //главная функция 
    $('.icon-menu').click(function() {  
        $('.menu').animate({right: '0px'}, 400, "easeInExpo"); 
    });
 
 
/* Закрытие меню */
 
    $('.icon-close').click(function() {  
        $('.menu').animate({right: '-285px'}, 400, "easeInExpo");    	    	  
    });
};
 
$(document).ready(sidebarMenu); /* как только страница полностью загрузится, будет
               вызвана функция main, отвечающая за работу меню */